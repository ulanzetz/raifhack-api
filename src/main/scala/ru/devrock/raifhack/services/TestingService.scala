package ru.devrock.raifhack.services

import ru.devrock.raifhack.Domain.Id
import ru.devrock.raifhack.repositories.testing.TestingDomain._
import ru.devrock.raifhack.repositories.testing.TestingRepository

trait TestingService[F[_]] {
  def addTesting(shopId: Id, req: AddTestingRequest): F[Unit]

  def list(shopId: Id): F[GetTestingListResponse]

  def disable(testingId: Id): F[Unit]
}

class TestingServiceImpl[F[_]](implicit repo: TestingRepository[F]) extends TestingService[F] {
  def addTesting(shopId: Id, req: AddTestingRequest): F[Unit] =
    repo.addTesting(shopId, req)

  def list(shopId: Id): F[GetTestingListResponse] =
    repo.list(shopId)

  def disable(testingId: Id): F[Unit] =
    repo.disable(testingId)
}
