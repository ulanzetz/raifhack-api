package ru.devrock.raifhack.services.auth

import derevo.circe.{decoder, encoder}
import derevo.derive
import ru.devrock.raifhack.CommonInstances
import ru.devrock.raifhack.Domain._
import ru.tinkoff.tschema.swagger.Swagger

object AuthServiceDomain extends CommonInstances {
  @derive(decoder, Swagger) final case class LoginConfirmRequest(phone: Phone, code: Code)

  @derive(encoder, Swagger) final case class LoginConfirmResponse(session: Session)

  @derive(encoder, Swagger) final case class SendCodeResponse(codeMaxMillis: Long, millisBetweenCodes: Long)
}
