package ru.devrock.raifhack.services.auth

import cats.effect.{Sync, Timer}
import cats.implicits._
import com.typesafe.scalalogging.LazyLogging
import ru.devrock.raifhack.Domain._
import ru.devrock.raifhack.http.SessionProvider
import ru.devrock.raifhack.repositories.{AuthRepository, UserRepository}
import ru.devrock.raifhack.services.auth.AuthServiceDomain._
import ru.devrock.raifhack.syntax.timer._

trait AuthService[F[_]] {
  def sendCode(phone: Phone): F[SendCodeResponse]

  def confirm(req: LoginConfirmRequest): F[LoginConfirmResponse]

  def hack(userId: UserId): F[LoginConfirmResponse]
}

class AuthServiceImpl[F[_]: Sync: Timer](cfg: AuthServiceConfig)(
    implicit repo: AuthRepository[F],
    userRepo: UserRepository[F],
    sessionProvider: SessionProvider
) extends AuthService[F] with LazyLogging {
  def sendCode(phone: Phone): F[SendCodeResponse] =
    for {
      maybeAuthInfo <- repo.get(phone)
      ts            <- Timer[F].millis
      _ <- maybeAuthInfo.fold(Sync[F].unit) { info =>
        val expired = ts - info.ts
        Sync[F]
          .raiseError(
            AppError(
              s"Таймаут для повторной отправки еще не вышел. Прошло: $expired. Необходимо: ${cfg.millisBetweenCodes}"
            )
          )
          .whenA(expired < cfg.millisBetweenCodes)
      }
      code <- Sync[F].delay(Code.create(cfg.maxCodeValue))
      _    <- repo.saveCode(phone, code)
    } yield SendCodeResponse(cfg.codeMaxMillis, cfg.millisBetweenCodes)

  def confirm(req: LoginConfirmRequest): F[LoginConfirmResponse] =
    for {
      maybeAuthInfo <- repo.get(req.phone)
      ts            <- Timer[F].millis
      res <- maybeAuthInfo.fold[F[Session]](Sync[F].raiseError(AppError("Нет информации о данном аккаунте"))) { info =>
        val passed = ts - info.ts

        if (passed >= cfg.codeMaxMillis)
          for {
            _ <- Sync[F].delay(logger.info(s"Code max millis: ${cfg.codeMaxMillis}. Passed: $passed"))
            err <- Sync[F]
              .raiseError[Session](AppError("Время до повторой отправки кода еще не прошло"))
          } yield err
        else if (req.code != info.code)
          for {
            _   <- Sync[F].delay(logger.info("Incorrect code: ${req.code.repr} != ${info.code}"))
            err <- Sync[F].raiseError[Session](AppError("Некорректный код"))
          } yield err
        else userRepo.getUserId(req.phone).map(sessionProvider.session)
      }
      _ <- repo.delete(req.phone)
    } yield LoginConfirmResponse(res)

  def hack(userId: UserId): F[LoginConfirmResponse] =
    Sync[F].delay(LoginConfirmResponse(sessionProvider.session(userId)))
}
