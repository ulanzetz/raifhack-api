package ru.devrock.raifhack.services.auth

case class AuthServiceConfig(codeMaxMillis: Long, millisBetweenCodes: Long, maxCodeValue: Int)
