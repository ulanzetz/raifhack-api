package ru.devrock.raifhack.services

import cats.Monad
import cats.implicits._
import ru.devrock.raifhack.Domain.{Id, UserId}
import ru.devrock.raifhack.repositories.order.OrderDomain._
import ru.devrock.raifhack.repositories.order.OrderRepository
import ru.devrock.raifhack.repositories.shop.ShopRepository
import ru.devrock.raifhack.repositories.testing.TestingRepository

trait OrderService[F[_]] {
  def positions(shopId: Id): F[PositionsResponse]

  def addToCart(cartId: Id, req: AddToCartRequest): F[Cart]

  def removeFromCart(cartId: Id, req: RemoveFromCartRequest): F[Cart]

  def clearCart(cartId: Id): F[Unit]

  def cart(cartId: Id): F[Cart]

  def createCart(shopId: Id): F[CreateCartResponse]

  def pay(userId: UserId, cartId: Id): F[Unit]

  def userCart(userId: UserId, cartId: Id): F[UserCart]
}

class OrderServiceImpl[F[_]: Monad](
    implicit repo: OrderRepository[F],
    shopRepo: ShopRepository[F],
    testingRepo: TestingRepository[F]
) extends OrderService[F] {
  def positions(shopId: Id): F[PositionsResponse] =
    repo.positions(shopId)

  def addToCart(cartId: Id, req: AddToCartRequest): F[Cart] =
    repo.addToCart(cartId, req) *> repo.cart(cartId)

  def removeFromCart(cartId: Id, req: RemoveFromCartRequest): F[Cart] =
    repo.removeFromCart(cartId, req) *> repo.cart(cartId)

  def clearCart(cartId: Id): F[Unit] =
    repo.clearCart(cartId)

  def cart(cartId: Id): F[Cart] =
    repo.cart(cartId)

  def createCart(shopId: Id): F[CreateCartResponse] =
    repo.createCart(shopId)

  def pay(userId: UserId, cartId: Id): F[Unit] =
    for {
      cartInfo <- cart(cartId)
      shop     <- repo.shopId(cartId)
      _        <- shopRepo.pay(userId, shop, cartInfo.price)
      _        <- testingRepo.update(userId, shop)
    } yield ()

  def userCart(userId: UserId, cartId: Id): F[UserCart] =
    for {
      cart   <- cart(cartId)
      shopId <- repo.shopId(cartId)
      offers <- shopRepo.offers(userId)
    } yield UserCart(cart, offers.list.filter(_.shop.id == shopId).map(_.offer))
}
