package ru.devrock.raifhack.services

import cats.effect.Sync
import ru.devrock.raifhack.Domain.{Id, UserId}
import ru.devrock.raifhack.repositories.UserRepository
import ru.devrock.raifhack.repositories.shop.ShopDomain._
import ru.devrock.raifhack.repositories.shop.ShopRepository

trait ShopService[F[_]] {
  def shops(userId: UserId): F[ShopsResponse]

  def shop(userId: UserId, shopId: Id): F[ShopFullInfo]

  def loyalty(shopId: Id): F[GetShopLoyalty]

  def setLoyalty(shopId: Id, loyalty: SetShopLoyalty): F[Unit]

  def offers(userId: UserId): F[ShopOffersResponse]

  def suggestOffers(shopId: Id): F[SuggestOffersResponse]

  def sendSuggestOffer(offerType: OfferType, offerId: Id): F[Unit]
}

class ShopServiceImpl[F[_]: Sync](implicit repo: ShopRepository[F], userRepo: UserRepository[F]) extends ShopService[F] {
  def shops(userId: UserId): F[ShopsResponse] =
    repo.shops(userId)

  def shop(userId: UserId, shopId: Id): F[ShopFullInfo] =
    repo.shop(userId, shopId)

  def loyalty(shopId: Id): F[GetShopLoyalty] =
    repo.loyalty(shopId)

  def setLoyalty(shopId: Id, loyalty: SetShopLoyalty): F[Unit] =
    repo.setLoyalty(shopId, loyalty)

  def offers(userId: UserId): F[ShopOffersResponse] =
    repo.offers(userId)

  def suggestOffers(shopId: Id): F[SuggestOffersResponse] =
    repo.suggestOffers(shopId)

  def sendSuggestOffer(offerType: OfferType, offerId: Id): F[Unit] =
    repo.sendSuggestOffer(offerType, offerId)
}
