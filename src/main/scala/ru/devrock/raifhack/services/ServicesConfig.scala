package ru.devrock.raifhack.services

import ru.devrock.raifhack.services.auth.AuthServiceConfig

case class ServicesConfig(auth: AuthServiceConfig)
