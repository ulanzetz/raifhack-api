package ru.devrock.raifhack.http

import akka.http.scaladsl.server.Directive1
import com.softwaremill.session.SessionOptions.{oneOff, usingHeaders}
import com.softwaremill.session._
import ru.devrock.raifhack.Domain.{Session, UserId}
import ru.devrock.raifhack.syntax.newtype._

import scala.util.Success

trait SessionProvider {
  def requiredSession: Directive1[UserId]

  def session(userId: UserId): Session
}

class SessionProviderImpl(cfg: SessionConfig) extends SessionProvider {
  import SessionProviderImpl.serializer

  private implicit val encoder: SessionEncoder[UserId] =
    com.softwaremill.session.SessionEncoder.basic

  private implicit val sessionManager: SessionManager[UserId] =
    new SessionManager[UserId](cfg)

  def requiredSession: Directive1[UserId] =
    com.softwaremill.session.SessionDirectives.requiredSession(oneOff, usingHeaders)

  def session(userId: UserId): Session =
    sessionManager.clientSessionManager.encode(userId).coerce[Session]
}

object SessionProviderImpl {
  implicit val serializer: SessionSerializer[UserId, String] =
    new SingleValueSessionSerializer[UserId, String](_.toString, s => Success(UserId(s.toInt)))
}
