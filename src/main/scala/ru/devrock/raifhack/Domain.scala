package ru.devrock.raifhack

import enumeratum.CirceEnum
import io.estatico.newtype.NewType

import scala.util.matching.Regex
import ru.devrock.raifhack.syntax.newtype._
import ru.tinkoff.tschema.param.HttpParam
import ru.tinkoff.tschema.swagger.SwaggerTypeable.SwaggerTypeableEnum

object Domain extends CommonInstances {
  type Id = Id.Type
  object Id extends NewType.Default[String] {
    def apply(num: Int): Type = wrap(num.toString)

    implicit final class Ops(private val self: Type) extends AnyVal {
      def internal: Int = unwrap(self).toInt
    }
  }

  type Session = Session.Type
  object Session extends NewType.Default[String]

  type Phone = Phone.Type
  object Phone extends NewType.Default[String] {
    val Regexp: Regex = "^(\\+7[0-9]{10})$".r
  }

  type UserId = UserId.Type
  object UserId extends NewType.Default[Int]

  type Code = Code.Type
  object Code extends NewType.Default[String] {
    def create(length: Int): Code = "1111".coerce[Code]
  }

  val RussianCapWordRegexp: Regex = "^([А-Я][а-я]{1,29})$".r

  type Name = Name.Type
  object Name extends NewType.Default[String]

  type Surname = Surname.Type
  object Surname extends NewType.Default[String]

  case class AppError(message: String, cause: Option[Throwable] = None) extends Exception

  final case class AuthInfo(code: Code, ts: Long)

  type Url = Url.Type
  object Url extends NewType.Default[String]

  trait Enum[E <: EnumEntry]
    extends enumeratum.Enum[E] with CirceEnum[E] with SwaggerTypeableEnum[E] with HttpParam.Enum[E] with PgEnum[E]

  val Enum = enumeratum.Enum

  type EnumEntry = enumeratum.EnumEntry
  val EnumEntry = enumeratum.EnumEntry

  type ZDT = java.time.ZonedDateTime

  type Date = java.time.LocalDate
  object Date {
    def parse(raw: String): Date = java.time.LocalDate.parse(raw)
  }
}
