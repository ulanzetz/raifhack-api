package ru.devrock.raifhack.tschema

import akka.http.scaladsl.server.Directives.complete
import akka.http.scaladsl.server.Route
import cats.syntax.option._
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.syntax._
import io.circe.{Encoder, Printer}
import monix.eval.Task
import ru.devrock.raifhack.Domain.{AppError, UserId}
import ru.devrock.raifhack.context.CoreContext
import ru.devrock.raifhack.http.SessionProvider
import ru.tinkoff.tschema.akkaHttp.{Routable, Serve}
import ru.tinkoff.tschema.common.Name
import ru.tinkoff.tschema.swagger.SwaggerMapper.swaggerAuth
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax.{complete => _, _}
import ru.tinkoff.tschema.typeDSL._
import shapeless.{HList, Witness}

object Instances extends LazyLogging {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)

  final class SessionAtom[name] extends DSLAtom with CanHoldApiKey

  implicit def apiKeyParam[name, x]: ApiKeyParam[SessionAtom[name], name, x] =
    ApiKeyParam(OpenApiParam.In.header)

  implicit def apiKeyAuthSwagger[realm: Name, name, x](
      implicit param: ApiKeyParam[SessionAtom[name], name, x]
  ): SwaggerMapper[ApiKeyAuth[realm, SessionAtom[name]]] =
    swaggerAuth[realm, x, ApiKeyAuth[realm, SessionAtom[name]]](
      typ = OpenApiSecurityType.apiKey,
      in = param.in.some,
      name = "Authorization".some
    )

  def session = apiKeyAuth("Raifhack Backend Session", sessionNamed('userId))

  private def sessionNamed[name <: Symbol](w: Witness.Lt[name]) =
    new SessionAtom[name]

  implicit def serveUserId[In <: HList, name](implicit provider: SessionProvider) =
    Serve.serveAdd[SessionAtom[name], In, UserId, name](provider.requiredSession)

  implicit def routable[A: Encoder](implicit provider: SessionProvider, core: CoreContext): Routable[Task[A], A] =
    new Routable[Task[A], A] {
      import akka.http.scaladsl.model._
      import akka.http.scaladsl.server.Directives.onComplete
      import core._

      import scala.util.{Failure, Success}

      override def route(res: => Task[A]): Route =
        onComplete(res.runToFuture) {
          case Success(value) =>
            complete(value.asJson)
          case Failure(e) =>
            val message = e match {
              case AppError(msg, _) =>
                msg
              case _ =>
                logger.error(e.toString)
                "Произошла внутренняя ошибка"
            }
            complete(HttpResponse(StatusCodes.InternalServerError, entity = message))
        }
    }
}
