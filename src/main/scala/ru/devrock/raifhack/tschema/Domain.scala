package ru.devrock.raifhack.tschema

import derevo.circe.encoder
import derevo.derive

object Domain {
  @derive(encoder)
  case class ErrorPayload(message: String)
}
