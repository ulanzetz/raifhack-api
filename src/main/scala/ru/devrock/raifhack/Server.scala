package ru.devrock.raifhack

import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.{Directive0, Route}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.server.RouteResult.{Complete, Rejected}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import cats.instances.list._
import cats.syntax.foldable._
import com.typesafe.scalalogging.LazyLogging
import monix.eval.Task
import ru.devrock.raifhack.context.CoreContext
import ru.devrock.raifhack.modules.Module
import ru.tinkoff.tschema.swagger.{OpenApi, OpenApiInfo, OpenApiServer}

class Server(modules: List[Module], cfg: ServerConfig)(implicit core: CoreContext) extends LazyLogging {
  import core._
  import ru.devrock.raifhack.tschema.Instances.printer

  val run: Task[Http.ServerBinding] = Task.deferFuture(Http().newServerAt(cfg.host, cfg.port).bindFlow(route))

  val info = OpenApiInfo(title = "DevRock Raifhack API", description = None, version = "0.1")

  private val apiServer = Vector(OpenApiServer(url = "/" + cfg.apiPath))

  private val static =
  pathEndOrSingleSlash(getFromResource("static/index.html")) ~
  path("swagger")(complete(swagger)) ~
  pathPrefix("static")(get(getFromResourceDirectory("static")))

  private val log: Directive0 =
    extractRequest.map(req => logger.info(s"Request: ${req.method} ${req.uri}")) &
    mapRouteResult { res =>
      res match {
        case Rejected(rejections) => logger.info(s"Rejected $rejections")
        case Complete(response) => logger.info("Responsed")
      }
      res
    }

  private val swagger: OpenApi = modules
    .foldMap(_.swagger)
    .make(info)
    .copy(servers = apiServer)

  private val corsHeaders: Directive0 =
    respondWithHeaders(
      `Access-Control-Allow-Origin`.*,
      `Access-Control-Allow-Credentials`(true),
      `Access-Control-Allow-Headers`("Authorization", "Content-Type", "X-Requested-With"),
      `Access-Control-Expose-Headers`("Set-Authorization")
    )


  private def corsRoutes: Route = options {
    complete(HttpResponse(StatusCodes.OK).withHeaders(`Access-Control-Allow-Methods`(OPTIONS, POST, PUT, GET, DELETE)))
  }

  private val api =
    (pathPrefix(cfg.apiPath) & corsHeaders)(corsRoutes ~ modules.map(_.route).reduce(_ ~ _))

  private val route = log(static ~ api)
}
