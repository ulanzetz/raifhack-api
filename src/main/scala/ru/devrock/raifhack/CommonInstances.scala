package ru.devrock.raifhack

import java.sql.Timestamp
import java.time.ZonedDateTime

import cats.syntax.option._
import eu.timepit.refined.api.Refined
import io.circe.syntax._
import io.circe.{Decoder, DecodingFailure, Encoder}
import io.estatico.newtype.{BaseNewType, Coercible}
import ru.devrock.raifhack.Domain._
import ru.devrock.raifhack.syntax.newtype._
import ru.tinkoff.tschema.param.{HttpSingleParam, ParseParamError}
import ru.tinkoff.tschema.swagger.{SwaggerPrimitive, SwaggerStringValue, SwaggerTypeable}

import scala.util.matching.Regex

trait CommonInstances extends LowPriorityInstances {
  implicit val phoneSwaggerTypeable: SwaggerTypeable[Phone] =
    SwaggerTypeable.make(new SwaggerPrimitive(SwaggerStringValue(pattern = Phone.Regexp.regex.some)))

  implicit val phoneDecoder: Decoder[Phone] = stringRegexpDecoder(Phone.Regexp)("Invalid phone format")

  implicit val phoneParser: HttpSingleParam[Phone] = HttpSingleParam.stringParam.applyOpt(_).flatMap {
    case Phone.Regexp(phone) => Right(phone.coerce[Phone])
    case _                   => Left(ParseParamError("Invalid phone format"))
  }

  implicit val nameDecoder: Decoder[Name] = stringRegexpDecoder(RussianCapWordRegexp)("Invalid name format")

  implicit val surnameDecoder: Decoder[Surname] = stringRegexpDecoder(RussianCapWordRegexp)("Invalid surname format")

  private def stringRegexpDecoder[T](
      regexp: Regex
  )(errorMsg: String = "Invalid format")(implicit C: Coercible[String, T]): Decoder[T] =
    _.as[String].flatMap {
      case regexp(valid) => Right(valid.coerce[T])
      case _             => Left(DecodingFailure(errorMsg, Nil))
    }

  implicit val ZDTSwagger: SwaggerTypeable[ZonedDateTime] = SwaggerTypeable.swaggerTypeableString.as[ZonedDateTime]

  implicit val TimestampSwagger: SwaggerTypeable[Timestamp] = SwaggerTypeable.swaggerTypeableString.as[Timestamp]

  implicit val TimestampEncoder: Encoder[Timestamp] =
    Encoder[Long].contramap(_.getTime / 1000)

  implicit val TimestampDecoder: Decoder[Timestamp] =
    Decoder[Long].map(ts => new Timestamp(ts))
}

trait LowPriorityInstances {
  implicit def newTypeEncoder[B, T, R: Encoder]: Encoder[BaseNewType.Aux[B, T, R]] =
    Encoder.instance[BaseNewType.Aux[B, T, R]](_.asInstanceOf[R].asJson)

  implicit def newTypeDecoder[B, T, R](implicit underlying: Decoder[R]): Decoder[BaseNewType.Aux[B, T, R]] =
    Decoder.instance[BaseNewType.Aux[B, T, R]](c => underlying(c).map(_.asInstanceOf[BaseNewType.Aux[B, T, R]]))

  implicit def newTypeSwagger[B, T, R](
      implicit underlying: SwaggerTypeable[R]
  ): SwaggerTypeable[BaseNewType.Aux[B, T, R]] =
    underlying.as[BaseNewType.Aux[B, T, R]]

  implicit def newTypeHttpParam[B, T, R](
      implicit underlying: HttpSingleParam[R]
  ): HttpSingleParam[BaseNewType.Aux[B, T, R]] =
    underlying.map(_.asInstanceOf[BaseNewType.Aux[B, T, R]])

  implicit def refinedSwagger[A, F](
      implicit underlying: SwaggerTypeable[A]
  ): SwaggerTypeable[A Refined F] =
    underlying.as[A Refined F]
}
