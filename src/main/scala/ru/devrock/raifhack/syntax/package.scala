package ru.devrock.raifhack

import cats.effect.Timer
import io.estatico.newtype.Coercible

import scala.concurrent.duration.MILLISECONDS

package object syntax {
  object newtype {
    implicit class NewTypeCoercibleIdOps[A](private val a: A) extends AnyVal {
      @inline def coerce[B](implicit C: Coercible[A, B]): B = C(a)
    }

    implicit class NewTypeCoercibleFOps[F[_], A](private val fa: F[A]) extends AnyVal {
      @inline def coerceM[B](implicit C: Coercible[F[A], F[B]]): F[B] = C(fa)
    }
  }

  object timer {
    implicit class TimerFunc[F[_]](private val timer: Timer[F]) extends AnyVal {
      @inline def millis: F[Long] = timer.clock.realTime(MILLISECONDS)
    }
  }
}
