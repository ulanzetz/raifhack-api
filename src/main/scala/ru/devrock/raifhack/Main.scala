package ru.devrock.raifhack

import akka.actor.ActorSystem
import com.softwaremill.session.SessionConfig
import doobie.util.transactor.Transactor
import monix.eval.Task
import monix.execution.Scheduler
import ru.devrock.raifhack.context.CoreContext
import ru.devrock.raifhack.db.AppTransactor
import ru.devrock.raifhack.http.{SessionProvider, SessionProviderImpl}
import ru.devrock.raifhack.modules._
import ru.devrock.raifhack.repositories._
import ru.devrock.raifhack.repositories.order.{OrderRepository, OrderRepositoryImpl}
import ru.devrock.raifhack.repositories.shop.{ShopRepository, ShopRepositoryImpl}
import ru.devrock.raifhack.repositories.testing.{TestingRepository, TestingRepositoryImpl}
import ru.devrock.raifhack.services.{OrderServiceImpl, ShopServiceImpl, TestingServiceImpl}
import ru.devrock.raifhack.services.auth.AuthServiceImpl

object Main {
  def main(args: Array[String]): Unit = {
    implicit val cfg: AppConfig                  = AppConfig.get()
    implicit val system: ActorSystem             = ActorSystem("devrock-raifhack", cfg.root)
    implicit val scheduler: Scheduler            = Scheduler(system.dispatcher)
    implicit val core: CoreContext               = new CoreContext
    implicit val sessionProvide: SessionProvider = new SessionProviderImpl(SessionConfig.fromConfig(cfg.root.get))

    implicit val transactor: Transactor[Task] = AppTransactor(cfg.db)

    //repos

    implicit val authRepo: AuthRepository[Task] = new AuthRepositoryImpl[Task]

    implicit val userRepo: UserRepository[Task] = new UserRepositoryImpl[Task]

    implicit val orderRepo: OrderRepository[Task] = new OrderRepositoryImpl[Task]

    implicit val shopRepo: ShopRepository[Task] = new ShopRepositoryImpl[Task]

    implicit val testingRepo: TestingRepository[Task] = new TestingRepositoryImpl[Task]

    // modules

    val authModule = new AuthModule(new AuthServiceImpl[Task](cfg.services.auth))

    val orderModule = new OrderModule(new OrderServiceImpl[Task])

    val shopModule = new ShopModule(new ShopServiceImpl[Task])

    val testingModule = new TestingModule(new TestingServiceImpl[Task])

    val modules = authModule :: orderModule :: shopModule :: testingModule :: Nil

    new Server(modules, cfg.server).run.runToFuture

    println(s"Started on ${cfg.server.port}")
  }
}
