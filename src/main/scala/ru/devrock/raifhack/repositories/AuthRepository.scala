package ru.devrock.raifhack.repositories

import cats.effect.{Sync, Timer}
import cats.implicits._
import doobie.implicits._
import doobie.util.transactor.Transactor
import ru.devrock.raifhack.Domain.{AuthInfo, Code, Phone}
import ru.devrock.raifhack.repositories.Instances._
import ru.devrock.raifhack.syntax.timer._

trait AuthRepository[F[_]] {
  def saveCode(phone: Phone, code: Code): F[Long]

  def get(phone: Phone): F[Option[AuthInfo]]

  def delete(phone: Phone): F[Unit]
}

class AuthRepositoryImpl[F[_]: Sync: Transactor: Timer] extends AuthRepository[F] {
  def saveCode(phone: Phone, code: Code): F[Long] =
    for {
      ts <- Timer[F].millis
      _  <- sql"""insert into auth (phone, code, ts) values ($phone, $code, $ts)
             on conflict (phone) do update set code = $code, ts = $ts""".update.run.to[F]
    } yield ts

  def get(phone: Phone): F[Option[AuthInfo]] =
    sql"select code, ts from auth where phone = $phone".query[AuthInfo].option.to[F]

  def delete(phone: Phone): F[Unit] =
    sql"delete from auth where phone = $phone".update.run.void.to[F]
}
