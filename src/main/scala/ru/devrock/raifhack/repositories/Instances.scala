package ru.devrock.raifhack.repositories

import cats.effect.Sync
import doobie.Meta
import doobie.implicits._
import doobie.free.connection.ConnectionIO
import doobie.util.fragment.Fragment
import doobie.util.transactor.Transactor
import eu.timepit.refined.api.Refined
import io.estatico.newtype.BaseNewType
import ru.devrock.raifhack.Domain.Id

object Instances extends LowPriorityInstances {
  implicit class ConnectionIOOps[A](connectionIO: ConnectionIO[A]) {
    def to[F[_]: Sync](implicit xa: Transactor[F]): F[A] =
      connectionIO.transact(xa)
  }

  implicit val idMeta: Meta[Id] =
    Meta.IntMeta.imap(Id(_))(_.internal)

  def mkFilter(fields: Option[Fragment]*): Fragment = {
    val nonEmpty = fields.flatten
    if (nonEmpty.isEmpty)
      Fragment.empty
    else
      nonEmpty.tail.foldLeft(fr"where" ++ nonEmpty.head)(_ ++ fr"and" ++ _)
  }
}

trait LowPriorityInstances {
  implicit def newTypeDoobieMeta[B, T, R](implicit underlying: Meta[R]): Meta[BaseNewType.Aux[B, T, R]] =
    underlying.asInstanceOf[Meta[BaseNewType.Aux[B, T, R]]]

  implicit def refinedDoobieMeta[A, F](implicit underlying: Meta[A]): Meta[A Refined F] =
    underlying.imap[A Refined F](Refined.unsafeApply)(_.value)
}