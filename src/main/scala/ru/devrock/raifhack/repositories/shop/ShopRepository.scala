package ru.devrock.raifhack.repositories.shop

import cats.effect.Sync
import cats.implicits._
import doobie.ConnectionIO
import doobie.util.transactor.Transactor
import doobie.implicits._
import io.scalaland.chimney.Transformer
import ru.devrock.raifhack.Domain.{Id, UserId}
import ru.devrock.raifhack.repositories.shop.ShopDomain._
import ru.devrock.raifhack.repositories.Instances._
import ru.devrock.raifhack.repositories.order.OrderDomain.Position
import io.scalaland.chimney.dsl._

trait ShopRepository[F[_]] {
  def shops(userId: UserId): F[ShopsResponse]

  def shop(userId: UserId, shopId: Id): F[ShopFullInfo]

  def loyalty(shopId: Id): F[GetShopLoyalty]

  def setLoyalty(shopId: Id, loyalty: SetShopLoyalty): F[Unit]

  def offers(userId: UserId): F[ShopOffersResponse]

  def recalcAll: F[Unit]

  def suggestOffers(shopId: Id): F[SuggestOffersResponse]

  def sendSuggestOffer(offerType: OfferType, offerId: Id): F[Unit]

  def pay(userId: UserId, shopId: Id, sum: Int): F[Unit]
}

class ShopRepositoryImpl[F[_]: Sync: Transactor] extends ShopRepository[F] {
  import ShopRepositoryImpl._

  // TODO optimize
  def shops(userId: UserId): F[ShopsResponse] =
    sql"""select s.id, s.name, sc.id, sc.name, sc.plural_name, sc.logo,
          s.loyalty_type, slc.category_type, slc.name, slc.sum, slc.percent,
          (select category_type from shop_loyalty_categories slc2 where slc2.shop_id = s.id and slc2.ord = slc.ord + 1),
          (select name from shop_loyalty_categories slc2 where slc2.shop_id = s.id and slc2.ord = slc.ord + 1),
          (select sum from shop_loyalty_categories slc2 where slc2.shop_id = s.id and slc2.ord = slc.ord + 1),
          (select percent from shop_loyalty_categories slc2 where slc2.shop_id = s.id and slc2.ord = slc.ord + 1),
          uss.cumulative_sum, uss.order_sum, uss.check_sum
          from shops s
          join shop_categories sc on sc.id = s.category_id
          left join user_shop_stats uss on s.id = uss.shop_id and uss.user_id = $userId
          left join shop_loyalty_categories slc on slc.id = uss.category_id
       """
      .query[RawShopShortInfo]
      .to[List]
      .to[F]
      .map(rawList => ShopsResponse(rawList.map(_.transformInto[ShopShortInfo])))

  // TODO optimize
  def shop(userId: UserId, shopId: Id): F[ShopFullInfo] =
    (for {
      raw <- sql"""
          select s.id, s.name, sc.id, sc.name, sc.plural_name, sc.logo,
          s.loyalty_type, slc.category_type, slc.name, slc.sum, slc.percent,
          (select category_type from shop_loyalty_categories slc2 where slc2.shop_id = s.id and slc2.ord = slc.ord + 1),
          (select name from shop_loyalty_categories slc2 where slc2.shop_id = s.id and slc2.ord = slc.ord + 1),
          (select sum from shop_loyalty_categories slc2 where slc2.shop_id = s.id and slc2.ord = slc.ord + 1),
          (select percent from shop_loyalty_categories slc2 where slc2.shop_id = s.id and slc2.ord = slc.ord + 1),
          uss.cumulative_sum, uss.order_sum, uss.check_sum, s.logo, s.address, s.subway
          from shops s
          join shop_categories sc on sc.id = s.category_id
          left join user_shop_stats uss on s.id = uss.shop_id and uss.user_id = $userId
          left join shop_loyalty_categories slc on slc.id = uss.category_id
          where s.id = $shopId
       """.query[RawShopFullInfo].unique
      freePositions <- sql"""select p.id, p.title, p.category, p.price 
              from user_shop_free_positions usfp join positions p on usfp.position_id = p.id
              where usfp.shop_id = $shopId and usfp.user_id = $userId"""
        .query[Position]
        .to[List]
      additionalSales <- sql"""select percent from user_shop_additional_sale where user_id = $userId and shop_id = $shopId"""
        .query[Int]
        .to[List]
    } yield
      raw
        .into[ShopFullInfo]
        .withFieldConst(
          _.offers,
          freePositions.map(p => Offer(offerType = OfferType.FreePosition, freePosition = p.some)) :::
          additionalSales.map(percent => Offer(offerType = OfferType.AdditionalSale, salePercent = percent.some))
        )
        .transform).to[F]

  def loyalty(shopId: Id): F[GetShopLoyalty] =
    (for {
      loyaltyType <- sql"select loyalty_type from shops where id = $shopId".query[LoyaltyType].unique
      categories <- sql"""select id, category_type, name, sum, percent, users_count, middle_check, orders_count
                          from shop_loyalty_categories where shop_id = $shopId"""
        .query[LoyaltyCategoryFullInfo]
        .to[List]
    } yield GetShopLoyalty(loyaltyType, categories)).to[F]

  def setLoyalty(shopId: Id, loyalty: SetShopLoyalty): F[Unit] =
    (for {
      _ <- sql"update shop_testings set category_id = null where shop_id = $shopId".update.run
      _ <- sql"update user_shop_stats set category_id = null where shop_id = $shopId".update.run
      _ <- sql"update suggest_shop_free_positions set category_id = null where shop_id = $shopId".update.run
      _ <- sql"update suggest_shop_additional_sale set category_id = null where shop_id = $shopId".update.run
      _ <- sql"update shops set loyalty_type = ${loyalty.loyaltyType} where id = $shopId".update.run
      _ <- sql"delete from shop_loyalty_categories where shop_id = $shopId".update.run
      ids <- loyalty.categories.traverseWithIndexM { (category, idx) =>
        for {
          id <- sql"""insert into shop_loyalty_categories (shop_id, name, category_type, percent, sum, ord)
              values ($shopId, ${category.categoryName}, ${category.categoryType},
                ${category.categoryPercent}, ${category.categorySum}, ${idx + 1}
              ) returning id""".query[Id].unique
          _ <- category.categoryType match {
            case LoyaltyCategoryType.CheckSum =>
              sql"""update user_shop_stats set category_id = $id where shop_id = $shopId and check_sum >= ${category.categorySum}""".update.run
            case LoyaltyCategoryType.OrderSum =>
              sql"""update user_shop_stats set category_id = $id where shop_id = $shopId and order_sum >= ${category.categorySum}""".update.run
          }
          _ <- sql"update shop_testings set category_id = $id where shop_id = $shopId and category_name = ${category.categoryName}".update.run
        } yield id
      }
      _ <- ids.traverse(recalcStats)
    } yield ()).to[F]

  def offers(userId: UserId): F[ShopOffersResponse] =
    (for {
      freePositions <- sql"""select s.id, s.name, sc.id,
              sc.name, sc.plural_name, sc.logo, p.id, p.title, p.category, p.price
              from user_shop_free_positions usfp 
              join positions p on usfp.position_id = p.id
              join shops s on s.id = usfp.shop_id
              join shop_categories sc on sc.id = s.category_id
              where usfp.user_id = $userId"""
        .query[(ShopInfo, Position)]
        .to[List]
      additionalSales <- sql"""select s.id, s.name, sc.id,
              sc.name, sc.plural_name, sc.logo, percent from user_shop_additional_sale usas
              join shops s on s.id = usas.shop_id
              join shop_categories sc on sc.id = s.category_id
              where user_id = $userId"""
        .query[(ShopInfo, Int)]
        .to[List]
    } yield
      ShopOffersResponse(
        freePositions.map {
          case (shop, p) => ShopOffer(shop, Offer(offerType = OfferType.FreePosition, freePosition = p.some))
        } :::
        additionalSales.map {
          case (shop, percent) =>
            ShopOffer(shop, Offer(offerType = OfferType.AdditionalSale, salePercent = percent.some))
        }
      )).to[F]

  def suggestOffers(shopId: Id): F[SuggestOffersResponse] =
    (for {
      freePositions   <- sql"""select ssfp.id, ssfp.category_name, p.id, p.title, p.category, p.price, ssfp.tested, ssfp.sent
              from suggest_shop_free_positions ssfp
              join positions p on ssfp.positions_id = p.id
              where ssfp.shop_id = $shopId""".query[RawSuggestFreePositions].to[List]
      additionalSales <- sql"""select ssas.id, ssas.category_name, ssas.sale_percent, ssas.tested, ssas.sent
               from suggest_shop_additional_sale ssas
               where ssas.shop_id = $shopId""".query[RawSuggestAdditionalSale].to[List]
    } yield
      SuggestOffersResponse(
        freePositions.map(
          p =>
            SuggestOffer(
              p.id,
              p.categoryName,
              Offer(OfferType.FreePosition, freePosition = p.position.some),
              p.tested,
              p.sent
          )
        ) :::
        additionalSales.map(
          s =>
            SuggestOffer(
              s.id,
              s.categoryName,
              Offer(OfferType.AdditionalSale, salePercent = s.salePercent.some),
              s.tested,
              s.sent
          )
        )
      )).to[F]

  def sendSuggestOffer(offerType: OfferType, offerId: Id): F[Unit] =
    offerType match {
      case OfferType.AdditionalSale =>
        sql"update suggest_shop_additional_sale set sent = true where id = $offerId".update.run.to[F].void
      case OfferType.FreePosition =>
        sql"update suggest_shop_free_positions set sent = true where id = $offerId".update.run.to[F].void
    }

  def recalcAll: F[Unit] =
    for {
      _ <- List(1, 2, 3, 4, 5).traverse { id =>
        loyalty(Id(id))
          .flatMap(loyalty => setLoyalty(Id(id), SetShopLoyalty(loyalty.loyaltyType, loyalty.categories.map(_.info))))
      }
      categoryIds <- sql"select id from shop_loyalty_categories".query[Id].to[List].to[F]
      _           <- categoryIds.traverse(recalcStats).to[F]
    } yield ()

  private def recalcStats(categoryId: Id): ConnectionIO[Unit] =
    for {
      stats <- sql"select count(*), coalesce(avg(avg_check), 0), coalesce(sum(orders_count), 0) from user_shop_stats where category_id = $categoryId"
        .query[LoyaltyCategoryStats]
        .unique
      _ <- sql"""update shop_loyalty_categories 
              set users_count = ${stats.usersCount}, middle_check = ${stats.middleCheck}, orders_count = ${stats.ordersCount}
              where id = $categoryId""".update.run
    } yield ()

  def pay(userId: UserId, shopId: Id, sum: Int): F[Unit] =
    for {
      _    <- sql"""insert into user_shop_stats (user_id, shop_id, avg_check) values
              ($userId, $shopId, 0) on conflict (user_id, shop_id) do nothing""".update.run.to[F]
      _    <- sql"""update user_shop_stats set orders_count = orders_count + 1,
                 check_sum = GREATEST(check_sum, $sum),
                 order_sum = order_sum + $sum,
                 avg_check = avg_check + 100
                 where user_id = $userId and shop_id = $shopId
                 """.update.run.to[F]
      shop <- shop(userId, shopId)
      _ <- (shop.info.loyalty.loyaltyType, shop.info.loyalty.loyaltyCategory.map(_.categoryPercent)) match {
        case (LoyaltyType.Cumulative, Some(percent)) =>
          sql"""update user_shop_stats set cumulative_sum = cumulative_sum + ${sum * percent / 100}
                where user_id = $userId and shop_id = $shopId""".update.run.to[F]
        case _ =>
          Sync[F].unit
      }
      categories <- sql"select id from shop_loyalty_categories where shop_id = $shopId".query[Id].to[List].to[F]
      _          <- categories.traverse(recalcStats).to[F]
    } yield ()
}

object ShopRepositoryImpl {
  case class RawLoyalty(
      loyaltyType: LoyaltyType,
      loyaltyCategory: Option[LoyaltyCategory],
      nextLoyaltyCategory: Option[LoyaltyCategory],
      cumulativeSum: Option[Int],
      orderSum: Option[Int],
      checkSum: Option[Int]
  )

  case class RawShopShortInfo(id: Id, name: String, category: ShopCategory, loyalty: RawLoyalty)

  case class RawShopFullInfo(info: RawShopShortInfo, logo: Option[String], address: String, subway: String)

  case class RawSuggestFreePositions(id: Id, categoryName: String, position: Position, tested: Boolean, sent: Boolean)

  case class RawSuggestAdditionalSale(id: Id, categoryName: String, salePercent: Int, tested: Boolean, sent: Boolean)

  implicit val LoyaltyT: Transformer[RawLoyalty, Loyalty] =
    Transformer
      .define[RawLoyalty, Loyalty]
      .withFieldComputed(
        _.bottomText,
        raw =>
          raw.nextLoyaltyCategory.flatMap(
            c =>
              c.categoryType match {
                case LoyaltyCategoryType.OrderSum =>
                  raw.orderSum.map(s => s"До следующего статуса - ${c.categorySum - s}")
                case LoyaltyCategoryType.CheckSum => s"До следующего статуса - Чек на ${c.categorySum}".some
            }
        )
      )
      .withFieldComputed(
        _.rightText,
        raw =>
          raw.loyaltyCategory.map(
            c =>
              raw.loyaltyType match {
                case LoyaltyType.Sale       => s"${c.categoryPercent} %"
                case LoyaltyType.Cumulative => s"${raw.cumulativeSum.getOrElse(0)} ₽"
            }
        )
      )
      .buildTransformer
}
