package ru.devrock.raifhack.repositories.shop

import derevo.circe.{decoder, encoder}
import derevo.derive
import ru.devrock.raifhack.CommonInstances
import ru.devrock.raifhack.Domain.{Enum, EnumEntry, Id}
import ru.devrock.raifhack.repositories.order.OrderDomain.Position
import ru.tinkoff.tschema.swagger.Swagger

import scala.collection.immutable

object ShopDomain extends CommonInstances {
  @derive(encoder, Swagger)
  case class ShopsResponse(list: List[ShopShortInfo])

  @derive(encoder, Swagger)
  case class ShopShortInfo(id: Id, name: String, category: ShopCategory, loyalty: Loyalty)

  @derive(encoder, Swagger)
  case class Loyalty(
      loyaltyType: LoyaltyType,
      loyaltyCategory: Option[LoyaltyCategory],
      nextLoyaltyCategory: Option[LoyaltyCategory],
      cumulativeSum: Option[Int],
      orderSum: Option[Int],
      checkSum: Option[Int],
      bottomText: Option[String],
      rightText: Option[String]
  )

  @derive(encoder, decoder, Swagger)
  case class LoyaltyCategory(categoryType: LoyaltyCategoryType, categoryName: String, categorySum: Int, categoryPercent: Int)

  @derive(encoder, Swagger)
  case class ShopFullInfo(info: ShopShortInfo, logo: Option[String], address: String, subway: String, offers: List[Offer])

  @derive(encoder, Swagger)
  case class Offer(offerType: OfferType, freePosition: Option[Position] = None, salePercent: Option[Int] = None)

  @derive(encoder, Swagger)
  case class ShopOffersResponse(list: List[ShopOffer])

  @derive(encoder, Swagger)
  case class ShopOffer(shop: ShopInfo, offer: Offer)

  @derive(encoder, Swagger)
  case class ShopInfo(id: Id, name: String, category: ShopCategory)

  @derive(encoder, Swagger)
  case class ShopCategory(id: Id, name: String, pluralName: String, logo: Option[String])

  sealed trait LoyaltyType extends EnumEntry
  object LoyaltyType extends Enum[LoyaltyType] {
    case object Sale       extends LoyaltyType
    case object Cumulative extends LoyaltyType

    def values: immutable.IndexedSeq[LoyaltyType] = findValues
  }

  sealed trait LoyaltyCategoryType extends EnumEntry
  object LoyaltyCategoryType extends Enum[LoyaltyCategoryType] {
    case object OrderSum extends LoyaltyCategoryType
    case object CheckSum extends LoyaltyCategoryType

    def values: immutable.IndexedSeq[LoyaltyCategoryType] = findValues
  }

  sealed trait OfferType extends EnumEntry
  object OfferType extends Enum[OfferType] {
    case object FreePosition extends OfferType
    case object AdditionalSale extends OfferType

    def values: immutable.IndexedSeq[OfferType] = findValues
  }

  @derive(encoder, Swagger)
  case class GetShopLoyalty(loyaltyType: LoyaltyType, categories: List[LoyaltyCategoryFullInfo])

  @derive(encoder, Swagger)
  case class LoyaltyCategoryFullInfo(id: Id, info: LoyaltyCategory, stats: LoyaltyCategoryStats)

  @derive(encoder, Swagger)
  case class LoyaltyCategoryStats(usersCount: Int, middleCheck: Double, ordersCount: Int)

  @derive(decoder, Swagger)
  case class SetShopLoyalty(loyaltyType: LoyaltyType, categories: List[LoyaltyCategory])

  @derive(encoder, Swagger)
  case class SuggestOffersResponse(list: List[SuggestOffer])

  @derive(encoder, Swagger)
  case class SuggestOffer(id: Id, categoryName: String, offer: Offer, tested: Boolean, sent: Boolean)
}
