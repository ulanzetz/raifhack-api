package ru.devrock.raifhack.repositories.order

import eu.timepit.refined.types.all.NonNegInt
import derevo.circe.{decoder, encoder}
import derevo.derive
import ru.devrock.raifhack.CommonInstances
import ru.devrock.raifhack.Domain.Id
import io.circe.refined._
import ru.devrock.raifhack.repositories.shop.ShopDomain.Offer
import ru.tinkoff.tschema.swagger.Swagger

object OrderDomain extends CommonInstances {
  @derive(encoder, Swagger) case class PositionsResponse(list: List[Position])

  @derive(encoder, Swagger) case class Position(id: Id, title: String, category: String, price: Int)

  @derive(decoder, Swagger) case class AddToCartRequest(positionId: Id, count: NonNegInt)

  @derive(decoder, Swagger) case class RemoveFromCartRequest(positionId: Id, count: NonNegInt)

  @derive(encoder, Swagger) case class Cart(list: List[CartItem], price: Int)

  @derive(encoder, Swagger) case class CartItem(position: Position, count: Int)

  @derive(encoder, Swagger) case class CreateCartResponse(cartId: Id)

  @derive(encoder, Swagger) case class UserCart(cart: Cart, offers: List[Offer])
}
