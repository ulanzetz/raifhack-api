package ru.devrock.raifhack.repositories.order

import cats.effect.Sync
import cats.implicits._
import doobie.implicits._
import doobie.util.transactor.Transactor
import ru.devrock.raifhack.Domain.Id
import ru.devrock.raifhack.repositories.order.OrderDomain._
import ru.devrock.raifhack.repositories.Instances._

trait OrderRepository[F[_]] {
  def positions(shopId: Id): F[PositionsResponse]

  def addToCart(cartId: Id, req: AddToCartRequest): F[Unit]

  def removeFromCart(cartId: Id, req: RemoveFromCartRequest): F[Unit]

  def clearCart(cartId: Id): F[Unit]

  def cart(cartId: Id): F[Cart]

  def createCart(shopId: Id): F[CreateCartResponse]

  def shopId(cartId: Id): F[Id]
}

class OrderRepositoryImpl[F[_]: Sync: Transactor] extends OrderRepository[F] {
  def positions(shopId: Id): F[PositionsResponse] =
    sql"select id, title, category, price from positions where shop_id = $shopId order by ord"
      .query[Position]
      .to[List]
      .to[F]
      .map(PositionsResponse(_))

  def addToCart(cartId: Id, req: AddToCartRequest): F[Unit] =
    sql"""insert into cart_items (cart_id, position_id, count, ord)
          values (
            $cartId,
            ${req.positionId}, 
            ${req.count},
            coalesce((select ord from cart_items where cart_id = $cartId order by ord limit 1), 0)
          )
          on conflict (cart_id, position_id) do update set count =
          (select count from cart_items where cart_id = $cartId and position_id = ${req.positionId}) + ${req.count}
          """.update.run.to[F].void

  def removeFromCart(cartId: Id, req: RemoveFromCartRequest): F[Unit] =
    (for {
      count <- sql"select count from cart_items where cart_id = $cartId and position_id = ${req.positionId}"
        .query[Int]
        .unique
      _ <- (if (count - req.count.value > 0)
              sql"update cart_items set count = count - ${req.count} where cart_id = $cartId and position_id = ${req.positionId}"
            else
              sql"delete from cart_items where cart_id = $cartId and position_id = ${req.positionId}").update.run
    } yield ()).to[F]

  def clearCart(cartId: Id): F[Unit] =
    sql"delete from cart_items where cart_id = $cartId".update.run.to[F].void

  def cart(cartId: Id): F[Cart] =
    sql"""select id, title, category, price, count from cart_items
          join positions p on p.id = cart_items.position_id
          where cart_id = $cartId"""
      .query[CartItem]
      .to[List]
      .to[F]
      .map(list => Cart(list, list.map(item => item.position.price * item.count).sum))

  def createCart(shopId: Id): F[CreateCartResponse] =
    sql"insert into carts (shop_id) values ($shopId) returning cart_id".query[Id].unique.to[F].map(CreateCartResponse(_))

  def shopId(cartId: Id): F[Id] =
    sql"select shop_id from carts where cart_id = $cartId".query[Id].unique.to[F]
}
