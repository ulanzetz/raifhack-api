package ru.devrock.raifhack.repositories

import cats.effect.Sync
import doobie.implicits._
import doobie.util.transactor.Transactor
import ru.devrock.raifhack.Domain.{Phone, UserId}
import ru.devrock.raifhack.repositories.Instances._

trait UserRepository[F[_]] {
  def getUserId(phone: Phone): F[UserId]
}

class UserRepositoryImpl[F[_]: Sync: Transactor] extends UserRepository[F] {
  def getUserId(phone: Phone): F[UserId] =
    sql"insert into users (phone) values ($phone) on conflict (phone) do update set phone = $phone returning id"
      .query[UserId]
      .unique
      .to[F]
}
