package ru.devrock.raifhack.repositories.testing

import cats.effect.Sync
import cats.implicits._
import doobie.implicits._
import doobie.implicits.javatime._
import doobie.util.transactor.Transactor
import ru.devrock.raifhack.Domain.{Date, Id, UserId}
import ru.devrock.raifhack.repositories.Instances._
import ru.devrock.raifhack.repositories.testing.TestingDomain._
import io.scalaland.chimney.dsl._

trait TestingRepository[F[_]] {
  def addTesting(shopId: Id, req: AddTestingRequest): F[Unit]

  def list(shopId: Id): F[GetTestingListResponse]

  def disable(testingId: Id): F[Unit]

  def update(userId: UserId, shopId: Id): F[Unit]
}

class TestingRepositoryImpl[F[_]: Sync: Transactor] extends TestingRepository[F] {
  import TestingRepositoryImpl._

  def addTesting(shopId: Id, req: AddTestingRequest): F[Unit] =
    (for {
      id <- sql"""insert into shop_testings (shop_id, reason, start_date, end_date, category_id, category_name)
              values ($shopId, ${req.reason}, ${req.startDate}, ${req.endDate},
              ${req.categoryId}, (select name from shop_loyalty_categories where id = ${req.categoryId})
              ) returning id""".query[Int].unique
      users <- sql"select user_id from user_shop_stats where shop_id = $shopId and category_id = ${req.categoryId} order by random()"
        .query[UserId]
        .to[Vector]
      usersPerGroup = (users.length / 2) / req.groups.length
      _ <- req.groups.traverseWithIndexM { (group, idx) =>
        for {
          groupId <- sql"""insert into shop_testing_groups (testing_id, name, sale_percent)
                  values ($id, ${group.name}, ${group.salePercent}) returning id""".query[Id].unique
          _ <- users.slice(idx * usersPerGroup, idx * usersPerGroup + usersPerGroup).traverse { userId =>
            sql"""insert into user_shop_testings (user_id, testing_id, testing_group_id) values ($userId, $id, $groupId)""".update.run *>
            sql"""insert into user_shop_additional_sale (user_id, shop_id, percent, testing_group_id)
                  values ($userId, $shopId, ${group.salePercent}, $groupId)""".update.run
          }
          middleCheck <- sql"""select coalesce(avg(uss.avg_check), 0) from shop_testing_groups stg
                  join user_shop_testings ust on ust.testing_group_id = stg.id
                  join user_shop_stats uss on uss.user_id = ust.user_id where stg.id = $groupId"""
            .query[Double]
            .unique
          _ <- sql"update shop_testing_groups set middle_check = $middleCheck where id = $groupId".update.run
        } yield ()
      }
    } yield ()).to[F]

  def list(shopId: Id): F[GetTestingListResponse] =
    (for {
      rawList <- sql"select id, reason, category_name, start_date, end_date, enabled from shop_testings where shop_id = $shopId"
        .query[RawTestingInfo]
        .to[List]
      list <- rawList.traverse { raw =>
        sql"select id, name, sale_percent, middle_check, check_increase_percent from shop_testing_groups where testing_id = ${raw.id}"
          .query[TestingGroup]
          .to[List]
          .map(groups => raw.into[TestingInfo].withFieldConst(_.groups, groups).transform)
      }
    } yield GetTestingListResponse(list)).to[F]

  def disable(testingId: Id): F[Unit] =
    (for {
      _ <- sql"update shop_testings set enabled = false where id = $testingId".update.run
      _ <- sql"delete from user_shop_additional_sale where testing_id = $testingId".update.run
    } yield ()).to[F]

  def update(userId: UserId, shopId: Id): F[Unit] =
    (for {
      testingGroups <- sql"select testing_group_id from user_shop_testings where user_id = $userId".query[Id].to[List]
      _ <- testingGroups.traverse { groupId =>
        for {
          middleCheck <- sql"""select coalesce(avg(uss.avg_check), 0) from shop_testing_groups stg
                  join user_shop_testings ust on ust.testing_group_id = stg.id
                  join user_shop_stats uss on uss.user_id = ust.user_id where stg.id = $groupId"""
            .query[Double]
            .unique
          _ <- sql"""update shop_testing_groups set middle_check = $middleCheck, check_increase_percent = ((middle_check - $middleCheck) / 100)
                      where id = $groupId""".update.run
        } yield ()
      }
    } yield ()).to[F]
}

object TestingRepositoryImpl {
  case class RawTestingInfo(
      id: Id,
      reason: ShopTestingReason,
      categoryName: String,
      startDate: Date,
      endDate: Date,
      enabled: Boolean
  )
}
