package ru.devrock.raifhack.repositories.testing

import derevo.circe.{decoder, encoder}
import derevo.derive
import ru.devrock.raifhack.CommonInstances
import ru.devrock.raifhack.Domain.{Date, Enum, EnumEntry, Id}
import ru.tinkoff.tschema.swagger.Swagger

import scala.collection.immutable

object TestingDomain extends CommonInstances {
  sealed trait ShopTestingReason extends EnumEntry
  object ShopTestingReason extends Enum[ShopTestingReason] {
    case object IncreaseMidCheck extends ShopTestingReason
    case object ReturnClients    extends ShopTestingReason
    case object IncreaseOrders   extends ShopTestingReason

    def values: immutable.IndexedSeq[ShopTestingReason] = findValues
  }

  @derive(decoder, Swagger)
  case class AddTestingRequest(
      reason: ShopTestingReason,
      categoryId: Id,
      startDate: Date,
      endDate: Date,
      groups: List[AddTestingGroup]
  )

  @derive(decoder, Swagger)
  case class AddTestingGroup(name: String, salePercent: Int)

  @derive(encoder, Swagger)
  case class GetTestingListResponse(list: List[TestingInfo])

  @derive(encoder, Swagger)
  case class TestingInfo(
      id: Id,
      reason: ShopTestingReason,
      categoryName: String,
      startDate: Date,
      endDate: Date,
      enabled: Boolean,
      groups: List[TestingGroup]
  )

  @derive(encoder, Swagger)
  case class TestingGroup(id: Id, name: String, salePercent: Int, middleCheck: Int, checkIncreasePercent: Double)
}
