package ru.devrock.raifhack.db

import java.util.concurrent.Executors

import cats.effect.Blocker
import com.zaxxer.hikari.HikariDataSource
import doobie.hikari.HikariTransactor
import monix.eval.Task
import ru.devrock.raifhack.DbConfig

import scala.concurrent.ExecutionContext

object AppTransactor {
  def apply(cfg: DbConfig): HikariTransactor[Task] = {
    val connectionPool = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(cfg.transactionPoolSize))

    val transactionPool = ExecutionContext.fromExecutor(Executors.newCachedThreadPool { (r: Runnable) =>
      val t = new Thread(r, "spp-transaction-pool")
      t.setDaemon(true)
      t
    })

    val dataSource = new HikariDataSource()

    dataSource.setJdbcUrl(cfg.host)
    dataSource.setUsername(cfg.user)
    dataSource.setPassword(cfg.password)
    dataSource.setMaximumPoolSize(cfg.maximumPoolSize)
    dataSource.setConnectionInitSql("SET TIME ZONE 'UTC'")

    HikariTransactor(dataSource, connectionPool, Blocker.liftExecutionContext(transactionPool))
  }
}
