package ru.devrock.raifhack.context

import akka.actor.ActorSystem
import akka.stream.Materializer
import monix.execution.Scheduler
import ru.devrock.raifhack.AppConfig

class CoreContext()(
    implicit val system: ActorSystem,
    val materializer: Materializer,
    val scheduler: Scheduler,
    val cfg: AppConfig
)
