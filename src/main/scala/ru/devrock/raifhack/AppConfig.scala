package ru.devrock.raifhack

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import net.ceedubs.ficus.FicusInstances
import net.ceedubs.ficus.Ficus.toFicusConfig
import net.ceedubs.ficus.readers.ArbitraryTypeReader._
import net.ceedubs.ficus.readers.ValueReader
import ru.devrock.raifhack.services.ServicesConfig

case class AppConfig(
    server: ServerConfig,
    db: DbConfig,
    services: ServicesConfig,
    root: Option[Config]
)

case class ServerConfig(host: String, port: Int, apiPath: String)

case class DbConfig(driver: String, host: String, user: String, password: String, maximumPoolSize: Int, transactionPoolSize: Int)

object AppConfig extends FicusInstances {
  implicit override val booleanValueReader: ValueReader[Boolean] =
    (cfg, path) => if (cfg.hasPath(path)) net.ceedubs.ficus.Ficus.booleanValueReader.read(cfg, path) else false

  implicit def setReader[A: ValueReader]: ValueReader[Set[A]] =
    (cfg, path) => if (cfg.hasPath(path)) traversableReader[Set, A].read(cfg, path) else Set.empty

  lazy val underlying: Config = {
    val ovrrd    = Option(System.getProperty("override.config.file")).map(path => ConfigFactory.parseFile(new File(path)))
    val existing = ConfigFactory.load()

    ovrrd.fold(existing)(_.withFallback(existing)).resolve()
  }

  def get(): AppConfig = underlying.as[AppConfig]("ru.devrock.raifhack").copy(root = Some(underlying))
}
