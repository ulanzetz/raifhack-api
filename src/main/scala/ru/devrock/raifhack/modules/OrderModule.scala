package ru.devrock.raifhack.modules

import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport.unmarshaller
import ru.devrock.raifhack.Domain._
import monix.eval.Task
import ru.devrock.raifhack.context.CoreContext
import ru.devrock.raifhack.http.SessionProvider
import ru.devrock.raifhack.repositories.order.OrderDomain._
import ru.devrock.raifhack.services.OrderService
import ru.devrock.raifhack.tschema.Instances._
import ru.tinkoff.tschema.akkaHttp.MkRoute
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

final class OrderModule(service: OrderService[Task])(implicit core: CoreContext, sessionProvider: SessionProvider)
  extends Module {
  import OrderModule._

  override def route: Route = MkRoute(api)(service)

  override def swagger: SwaggerBuilder = MkSwagger(api)
}

object OrderModule {
  private def api =
    tag('Order) |>
    (
      positions <|>
      addToCart <|>
      removeFromCart <|>
      clearCart <|>
      cart <|>
      createCart <|>
      pay <|>
      userCart
    )

  private def positions =
    get |>
    keyPrefix('positions) |>
    capture[Id]('shopId) |>
    $$[PositionsResponse]

  private def addToCart =
    post |>
    prefix('cart) |>
    capture[Id]('cartId) |>
    prefix('add) |>
    key('addToCart) |>
    body[AddToCartRequest]('req) |>
    $$[Cart]

  private def removeFromCart =
    post |>
    prefix('cart) |>
    capture[Id]('cartId) |>
    prefix('remove) |>
    key('removeFromCart) |>
    body[RemoveFromCartRequest]('req) |>
    $$[Cart]

  private def clearCart =
    post |>
    prefix('cart) |>
    capture[Id]('cartId) |>
    prefix('clear) |>
    key('clearCart) |>
    $$[Unit]

  private def cart =
    get |>
    keyPrefix('cart) |>
    capture[Id]('cartId) |>
    prefix('info) |>
    $$[Cart]

  private def userCart =
    get |>
    prefix('cart) |>
    capture[Id]('cartId) |>
    prefix('user) |>
    prefix('info) |>
    key('userCart) |>
    session |>
    $$[UserCart]

  private def createCart =
    post |>
    prefix('create) |>
    prefix('cart) |>
    capture[Id]('shopId) |>
    key('createCart) |>
    $$[CreateCartResponse]

  private def pay =
    post |>
    prefix('cart) |>
    capture[Id]('cartId) |>
    keyPrefix('pay) |>
    session |>
    $$[Unit]
}
