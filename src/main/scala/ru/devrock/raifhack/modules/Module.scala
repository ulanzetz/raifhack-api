package ru.devrock.raifhack.modules

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import ru.devrock.raifhack.CommonInstances
import ru.tinkoff.tschema.swagger.{MkSwagger, SwaggerBuilder}

trait Module extends CommonInstances {
  def route: Route            = reject
  def swagger: SwaggerBuilder = MkSwagger.empty
}
