package ru.devrock.raifhack.modules

import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport.unmarshaller
import ru.devrock.raifhack.Domain._
import monix.eval.Task
import ru.devrock.raifhack.context.CoreContext
import ru.devrock.raifhack.http.SessionProvider
import ru.devrock.raifhack.repositories.shop.ShopDomain._
import ru.devrock.raifhack.services.ShopService
import ru.devrock.raifhack.tschema.Instances._
import ru.tinkoff.tschema.akkaHttp.MkRoute
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

final class ShopModule(service: ShopService[Task])(implicit core: CoreContext, sessionProvider: SessionProvider)
  extends Module {
  import ShopModule._

  override def route: Route = MkRoute(api)(service)

  override def swagger: SwaggerBuilder = MkSwagger(api)
}

object ShopModule {
  private def api =
    tag('Shop) |>
    (
      shops <|>
      shop <|>
      offers <|>
      loyalty <|>
      setLoyalty <|>
      suggestOffers <|>
      sendSuggestOffer
    )

  private def shops =
    get |>
    prefix('shop) |>
    prefix('list) |>
    key('shops) |>
    session |>
    $$[ShopsResponse]

  private def shop =
    get |>
    prefix('shop) |>
    capture[Id]('shopId) |>
    key('shop) |>
    session |>
    $$[ShopFullInfo]

  private def offers =
    get |>
    keyPrefix('offers) |>
    session |>
    $$[ShopOffersResponse]

  private def loyalty =
    get |>
    prefix('shop) |>
    capture[Id]('shopId) |>
    keyPrefix('loyalty) |>
    $$[GetShopLoyalty]

  private def setLoyalty =
    post |>
    prefix('shop) |>
    capture[Id]('shopId) |>
    prefix('loyalty) |>
    body[SetShopLoyalty]('loyalty) |>
    key('setLoyalty) |>
    $$[Unit]

  private def suggestOffers =
    get |>
    prefix('shop) |>
    capture[Id]('shopId) |>
    prefix('suggest) |>
    prefix('offers) |>
    key('suggestOffers) |>
    $$[SuggestOffersResponse]

  private def sendSuggestOffer =
    post |>
    prefix('shop) |>
    capture[Id]('shopId) |>
    prefix('suggest) |>
    prefix('offers) |>
    capture[Id]('offerId) |>
    prefix('send) |>
    key('sendSuggestOffer) |>
    queryParam[OfferType]('offerType) |>
    $$[Unit]
}
