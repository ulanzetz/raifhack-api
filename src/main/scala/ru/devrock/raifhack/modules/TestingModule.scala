package ru.devrock.raifhack.modules

import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport.unmarshaller
import ru.devrock.raifhack.Domain._
import monix.eval.Task
import ru.devrock.raifhack.context.CoreContext
import ru.devrock.raifhack.http.SessionProvider
import ru.devrock.raifhack.repositories.testing.TestingDomain._
import ru.devrock.raifhack.services.TestingService
import ru.devrock.raifhack.tschema.Instances._
import ru.tinkoff.tschema.akkaHttp.MkRoute
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

final class TestingModule(service: TestingService[Task])(implicit core: CoreContext, sessionProvider: SessionProvider)
  extends Module {
  import TestingModule._

  override def route: Route = MkRoute(api)(service)

  override def swagger: SwaggerBuilder = MkSwagger(api)
}

object TestingModule {
  private def api =
    tag('Testing) |>
    (
      addTesting <|>
      list <|>
      disable
    )

  private def addTesting =
    post |>
    prefix('testing) |>
    capture[Id]('shopId) |>
    prefix('add) |>
    key('addTesting) |>
    body[AddTestingRequest]('req) |>
    $$[Unit]

  private def list =
    get |>
    prefix('testing) |>
    capture[Id]('shopId) |>
    keyPrefix('list) |>
    $$[GetTestingListResponse]

  private def disable =
    post |>
    prefix('testing) |>
    keyPrefix('disable) |>
    capture[Id]('testingId) |>
    $$[Unit]
}
