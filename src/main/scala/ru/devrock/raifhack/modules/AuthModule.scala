package ru.devrock.raifhack.modules

import akka.http.scaladsl.server.Route
import ru.devrock.raifhack.Domain._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport.unmarshaller
import monix.eval.Task
import ru.devrock.raifhack.context.CoreContext
import ru.devrock.raifhack.http.SessionProvider
import ru.devrock.raifhack.services.auth.AuthService
import ru.devrock.raifhack.services.auth.AuthServiceDomain._
import ru.devrock.raifhack.tschema.Instances._
import ru.tinkoff.tschema.akkaHttp.MkRoute
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

final class AuthModule(service: AuthService[Task])(implicit core: CoreContext, sessionProvider: SessionProvider)
  extends Module {
  import AuthModule._

  override def route: Route = MkRoute(api)(service)

  override def swagger: SwaggerBuilder = MkSwagger(api)
}

object AuthModule {
  private def api = tag('Auth) |> prefix('auth) |> (confirm <|> sendCode <|> hack)

  private def hack =
    get |>
    keyPrefix('hack) |>
    capture[UserId]('userId) |>
    $$[LoginConfirmResponse]

  private def confirm =
    post |>
    keyPrefix('confirm) |>
    body[LoginConfirmRequest]('req) |>
    $$[LoginConfirmResponse]

  private def sendCode =
    post |>
    prefix('sendCode) |>
    key('sendCode) |>
    queryParam[Phone]('phone) |>
    $$[SendCodeResponse]
}
