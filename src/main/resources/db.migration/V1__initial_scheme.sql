create table auth
(
    phone varchar(13) not null,
    code  varchar(10) not null,
    ts    bigint      not null
);

create unique index auth_phone_uindex
    on auth (phone);

alter table auth
    add constraint auth_pk
        primary key (phone);

create table users
(
    id serial not null,
    phone varchar(20) not null
);

create unique index users_id_uindex
    on users (id);

create unique index users_phone_uindex
    on users (phone);

create table positions
(
    id serial not null,
    shop_id int not null,
    title varchar(500) not null,
    price int not null,
    category varchar(100) not null,
    ord smallint not null
);

create unique index positions_id_uindex
    on positions (id);

create index positions_shop_id_index
    on positions (shop_id);

alter table positions
    add constraint positions_pk
        primary key (id);

create table cart_items
(
    user_id int not null
        constraint cart_items_users_id_fk
            references users (id),
    position_id int not null
        constraint cart_items_positions_id_fk
            references positions,
    shop_id int not null,
    count int not null,
    ord smallint not null,
    constraint cart_items_pk
        primary key (user_id, position_id)
);

create index cart_items_user_id_index
    on cart_items (user_id);

create index cart_items_shop_id_index
    on cart_items (shop_id);

create table shops
(
    id serial not null
);

create unique index shops_id_uindex
    on shops (id);

alter table shops
    add constraint shops_pk
        primary key (id);

alter table positions
    add constraint positions_shops_id_fk
        foreign key (shop_id) references shops;

alter table cart_items
    add constraint cart_items_shops_id_fk
        foreign key (shop_id) references shops;


create table shop_categories
(
    id serial not null,
    name varchar(100) not null,
    logo text
);

create unique index shop_categories_id_uindex
    on shop_categories (id);

alter table shop_categories
    add constraint shop_categories_pk
        primary key (id);

alter table shop_categories
    add plural_name varchar(100) default 'Кофейни' not null;

create type loyalty_type as enum ('Sale', 'Cumulative');

alter table shops
    add category_id int default 1 not null;

alter table shops
    add loyalty_type loyalty_type default 'Sale';

alter table shops
    add constraint shops_shop_categories_id_fk
        foreign key (category_id) references shop_categories;


alter table shops
    add name varchar(100) default 'Coffee Shop';

alter table shops
    add logo text;

alter table shops
    add address text default 'г. Москва, ул. Пушкина, дом 4' not null;

alter table shops
    add subway varchar(100) default 'Беляево';

create type loyalty_category_type as enum ('OrderSum', 'CheckSum');

create table shop_loyalty_categories
(
    shop_id int not null
        constraint shop_loyalty_categories_shops_id_fk
            references shops,
    name varchar(100) not null,
    category_type loyalty_category_type not null,
    percent int not null,
    ord smallint not null
);

create index shop_loyalty_categories_shop_id_index
    on shop_loyalty_categories (shop_id);


alter table shop_loyalty_categories
    add id serial not null;

create unique index shop_loyalty_categories_id_uindex
    on shop_loyalty_categories (id);

alter table shop_loyalty_categories
    add constraint shop_loyalty_categories_pk
        primary key (id);

create table user_shop_stats
(
    user_id int not null
        constraint user_shop_stats_users_id_fk
            references users (id),
    shop_id int not null
        constraint user_shop_stats_shops_id_fk
            references shops,
    category_id int default null,
    order_sum int default 0 not null,
    check_sum int default 0 not null,
    cumulative_sum int default 0 not null
);

create unique index user_shop_stats_user_id_shop_id_uindex
    on user_shop_stats (user_id, shop_id);

alter table user_shop_stats
    add constraint user_shop_stats_pk
        primary key (user_id, shop_id);

create table user_shop_free_positions
(
    user_id int not null
        constraint user_shop_free_positions_users_id_fk
            references users (id),
    shop_id int not null
        constraint user_shop_free_positions_shops_id_fk
            references shops,
    position_id int not null
        constraint user_shop_free_positions_positions_id_fk
            references positions
);

create index user_shop_free_positions_user_id_shop_id_index
    on user_shop_free_positions (user_id, shop_id);

alter table shop_loyalty_categories
    add sum int default 1000 not null;

create table user_shop_additional_sale
(
    user_id int not null
        constraint user_shop_additional_sale_users_id_fk
            references users (id),
    shop_id int not null
        constraint user_shop_additional_sale_shops_id_fk
            references shops,
    percent int not null
);

create index user_shop_additional_sale_user_id_index
    on user_shop_additional_sale (user_id);

create index user_shop_additional_sale_user_id_shop_id_index
    on user_shop_additional_sale (user_id, shop_id);

create index user_shop_free_positions_user_id_index
    on user_shop_free_positions (user_id);

alter table user_shop_stats
    add orders_count int default 2 not null;

alter table shop_loyalty_categories
    add users_count int default 0 not null;

alter table shop_loyalty_categories
    add middle_check int default 0 not null;

alter table shop_loyalty_categories
    add orders_count int default 0 not null;

create table user_transactions
(
    id serial not null,
    user_id int not null
        constraint user_transactions_users_id_fk
            references users (id),
    datetime timestamp with time zone not null,
    amount int not null
);

create unique index user_transactions_id_uindex
    on user_transactions (id);

create index user_transactions_user_id_index
    on user_transactions (user_id);

alter table user_transactions
    add constraint user_transactions_pk
        primary key (id);

create type shop_testing_reason as enum ('IncreaseMidCheck', 'ReturnClients', 'IncreaseOrders')

create table shop_testings
(
    id serial not null,
    shop_id int not null
        constraint shop_testings_shops_id_fk
            references shops,
    reason shop_testing_reason not null,
    start_date date not null,
    end_date date not null,
    category_id int
        constraint shop_testings_shop_loyalty_categories_id_fk
            references shop_loyalty_categories,
    category_name varchar not null,
    enabled bool default true not null
);

create unique index shop_testings_id_uindex
    on shop_testings (id);

create index shop_testings_shop_id_index
    on shop_testings (shop_id);

alter table shop_testings
    add constraint shop_testings_pk
        primary key (id);


create table shops_testing_groups
(
    id serial not null,
    testing_id int not null
        constraint shops_testing_groups_shop_testings_id_fk
            references shop_testings,
    name varchar(500) not null,
    sale_percent int not null
);

create unique index shops_testing_groups_id_uindex
    on shops_testing_groups (id);

create index shops_testing_groups_testing_id_index
    on shops_testing_groups (testing_id);

alter table shops_testing_groups
    add constraint shops_testing_groups_pk
        primary key (id);

alter table shops_testing_groups rename to shop_testing_groups;

alter table shop_testing_groups
    add middle_check int default 0 not null;

alter table shop_testing_groups
    add check_increase_percent int default 0 not null;

alter table shop_testing_groups alter column check_increase_percent type double precision using check_increase_percent::double precision;

create table user_shop_testings
(
    user_id int not null
        constraint user_shop_testings_users_id_fk
            references users (id),
    testing_id int not null
        constraint user_shop_testings_shop_testings_id_fk
            references shop_testings,
    constraint user_shop_testings_pk
        primary key (user_id, testing_id)
);

create index user_shop_testings_testing_id_index
    on user_shop_testings (testing_id);

alter table user_shop_testings
    add testing_group_id int not null;

create index user_shop_testings_testing_group_id_index
    on user_shop_testings (testing_group_id);

alter table user_shop_testings
    add constraint user_shop_testings_shop_testing_groups_id_fk
        foreign key (testing_group_id) references shop_testing_groups;

alter table user_shop_additional_sale
    add testing_group_id int;

alter table user_shop_additional_sale
    add constraint user_shop_additional_sale_shop_testing_groups_id_fk
        foreign key (testing_group_id) references shop_testing_groups;

alter table user_shop_stats
    add avg_check double precision;

update user_shop_stats set avg_check = check_sum;

alter table user_shop_stats alter column avg_check set not null;

alter table user_shop_additional_sale
    add testing_id int;

alter table user_shop_additional_sale
    add constraint user_shop_additional_sale_shop_testings_id_fk
        foreign key (testing_id) references shop_testings;

update user_shop_additional_sale set testing_id = (select testing_id from shop_testing_groups stg where stg.id = user_shop_additional_sale.testing_group_id);


create table suggest_shop_additional_sale
(
    shop_id int not null
        constraint suggest_shop_additional_sale_shops_id_fk
            references shops,
    category_id int not null
        constraint suggest_shop_additional_sale_shop_loyalty_categories_id_fk
            references shop_loyalty_categories,
    sale_percent int not null
);

create index suggest_shop_additional_sale_shop_id_index
    on suggest_shop_additional_sale (shop_id);

alter table suggest_shop_additional_sale
    add id serial not null;

create unique index suggest_shop_additional_sale_id_uindex
    on suggest_shop_additional_sale (id);

alter table suggest_shop_additional_sale
    add constraint suggest_shop_additional_sale_pk
        primary key (id);

create table suggest_shop_free_positions
(
    id serial not null,
    positions_id int not null
        constraint sugges_shop_free_positions_positions_id_fk
            references positions,
    category_id int not null
        constraint sugges_shop_free_positions_shop_loyalty_categories_id_fk
            references shop_loyalty_categories,
    shop_id int not null
        constraint suggest_shop_free_positions_shops_id_fk
            references shops
);

create index sugges_shop_free_positions_shop_id_index
    on suggest_shop_free_positions (shop_id);

create unique index suggest_shop_free_positions_id_uindex
    on suggest_shop_free_positions (id);

alter table suggest_shop_free_positions
    add constraint suggest_shop_free_positions_pk
        primary key (id);

alter table suggest_shop_additional_sale
    add category_name varchar(100) not null;

alter table suggest_shop_free_positions
    add category_name varchar(100) not null;

alter table suggest_shop_free_positions
    add tested bool not null;

alter table suggest_shop_free_positions
    add sent bool not null;

alter table suggest_shop_additional_sale
    add tested bool not null;

alter table suggest_shop_additional_sale
    add sent bool not null;


alter table suggest_shop_additional_sale alter column category_id drop not null;

alter table suggest_shop_free_positions alter column category_id drop not null;

create table carts
(
    cart_id serial not null,
    shop_id int not null
        constraint carts_shops_id_fk
            references shops
);

create unique index carts_cart_id_uindex
    on carts (cart_id);

alter table carts
    add constraint carts_pk
        primary key (cart_id);

drop table cart_items;

create table cart_items
(
    cart_id int not null
        constraint cart_items_carts_cart_id_fk
            references carts,
    position_id int not null
        constraint cart_items_positions_id_fk
            references positions,
    count int not null,
    ord smallint not null
);

create index cart_items_cart_id_index
    on cart_items (cart_id);

alter table user_shop_stats alter column category_id set default null;

alter table user_shop_stats alter column avg_check set default 0;