import Dependencies._
import sbt._

enablePlugins(JavaAppPackaging)

name in Universal := "raifhack"
packageName in Universal := "raifhack"

lazy val commonSettings = Seq(
  organization := "ru.devrock",
  scalaVersion := "2.12.10",
  scalacOptions += "-Ywarn-unused:imports",
  sources in (Compile, doc) := Seq.empty,
  libraryDependencies ++= all
)

lazy val macros = (project in file("macros"))
  .settings(commonSettings)

lazy val raifhack = (project in file("."))
  .settings(commonSettings)
  .dependsOn(macros)
  .aggregate(macros)
  .settings(version := "0.1")