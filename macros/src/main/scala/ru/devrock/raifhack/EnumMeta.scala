package ru.devrock.raifhack

import enumeratum.{Enum, EnumEntry}

import scala.reflect.macros.blackbox

trait PgEnum[E <: EnumEntry] { self: Enum[E] =>
  implicit def meta: doobie.Meta[E] =
  macro EnumMeta.impl[E]
}

class EnumMeta(val c: blackbox.Context) {
  def impl[E: c.WeakTypeTag]: c.Expr[doobie.Meta[E]] = {
    import c.universe._
    val enum             = weakTypeOf[E].typeSymbol
    val postgresEnumName = snakeCase(enum.name.toString)
    val companion        = enum.companion

    c.Expr[doobie.Meta[E]](q"""_root_.doobie.postgres.implicits.pgEnumString(
         $postgresEnumName,
         s => $companion.withNameInsensitive(s).asInstanceOf[$enum],
         _.entryName.capitalize
       )""")
  }

  private[this] def snakeCase(string: String): String =
    string
      .foldLeft(("", true)) {
        case ((acc, prevUpper), current) =>
          val curUpper = current.isUpper
          val curLower = current.toLower
          if (curUpper && !prevUpper)
            (acc + s"_$curLower", curUpper)
          else (acc + curLower, curUpper)
      }
      ._1
}
