scalaVersion in ThisBuild := "2.12.8"

scalacOptions in ThisBuild ++= Seq(
  "-Ypartial-unification",
  "-unchecked",
  "-deprecation",
  "-feature",
  "-Ywarn-dead-code",
  "-Yno-adapted-args",
  "-language:existentials",
  "-language:experimental.macros",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:postfixOps"
)

libraryDependencies in ThisBuild ++= Seq(
  compilerPlugin("org.spire-math" %% "kind-projector" % "0.9.9"),
  compilerPlugin("org.scalamacros" % "paradise_2.12.8" % "2.1.1"),
  compilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.0-M4")
)