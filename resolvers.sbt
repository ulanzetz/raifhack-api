resolvers in ThisBuild ++= Seq(
  Resolver.sonatypeRepo("releases"),
  Resolver.sonatypeRepo("public"),
  Resolver.sonatypeRepo("snapshots"),
  Resolver.bintrayIvyRepo("scalameta", "maven"),
  Resolver.bintrayRepo("webjars", "maven"),
)