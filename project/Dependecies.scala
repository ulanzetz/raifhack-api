import sbt._

object Dependencies {
  private object version {
    val akkaHttp        = "10.1.11"
    val akkaHttpSession = "0.5.5"

    val circe           = "0.11.1"
    val circeDerivation = "0.11.0-M1"
    val circeAkka       = "1.25.2"

    val monix = "3.0.0-RC2"
    val ficus = "1.4.4"

    val doobie = "0.8.8"

    val scalaLogging = "3.9.0"
    val logback      = "1.2.3"

    val catsCore   = "2.1.0"
    val catsEffect = "2.0.0"

    val shapeless = "2.3.3"
    val magnolia  = "0.10.0"

    val newType         = "0.4.2"
    val enumeratum      = "1.5.13"
    val enumeratumCirce = "1.5.18"
    val monocle         = "1.6.0-M1"

    val scalameta   = "4.0.0"
    val typedSchema = "0.12.7"
    val derevo      = "0.11.5"

    val chimney = "0.6.1"
  }

  lazy val all = circe ++ monix ++
  cats ++ shapeless ++ magnolia ++
  newType ++ enumeratum ++ monocle ++ derevo ++
  akkaHttpCirce ++ doobie ++ scalaLogging ++ logback ++ typedSchema ++ akkaHttp ++ ficus ++ chimney

  lazy val akkaHttpCore    = lib("com.typesafe.akka", _.akkaHttp)("akka-http")
  lazy val akkaHttpCirce   = lib("de.heikoseeberger", _.circeAkka)("akka-http-circe")
  lazy val akkaHttpSession = lib("com.softwaremill.akka-http-session", _.akkaHttpSession)("core")
  lazy val akkaHttp        = akkaHttpCore ++ akkaHttpCirce ++ akkaHttpSession

  lazy val circeAll =
    lib("io.circe", _.circe, "circe")("core", "parser", "generic", "refined", "generic-extras", "java8")
  lazy val circeDerivation = lib("io.circe", _.circeDerivation, "circe")("derivation")
  lazy val circe           = circeAll ++ circeDerivation

  lazy val monix = lib("io.monix", _.monix)("monix")
  lazy val ficus = lib("com.iheart", _.ficus)("ficus")

  lazy val doobie = lib("org.tpolecat", _.doobie, "doobie")("core", "postgres", "hikari")

  lazy val scalaLogging = lib("com.typesafe.scala-logging", _.scalaLogging)("scala-logging")

  lazy val logback = jlib("ch.qos.logback", _.logback, "logback")("classic")

  lazy val catsCore   = lib("org.typelevel", _.catsCore, "cats")("core")
  lazy val catsEffect = lib("org.typelevel", _.catsEffect, "cats")("effect")
  lazy val cats       = catsCore ++ catsEffect

  lazy val shapeless = lib("com.chuusai", _.shapeless)("shapeless")
  lazy val magnolia  = lib("com.propensive", _.magnolia)("magnolia")

  lazy val newType = lib("io.estatico", _.newType)("newtype")
  lazy val monocle = lib("com.github.julien-truffaut", _.monocle, "monocle")("core", "macro", "state")

  lazy val enumeratumCore  = lib("com.beachape", _.enumeratum)("enumeratum")
  lazy val enumeratumCirce = lib("com.beachape", _.enumeratumCirce, "enumeratum")("circe")
  lazy val enumeratum      = enumeratumCore ++ enumeratumCirce

  lazy val scalameta = lib("org.scalameta", _.scalameta)("scalameta")

  lazy val typedSchema = lib("ru.tinkoff", _.typedSchema, "typed-schema")("swagger", "param", "akka-http")
  lazy val derevo      = lib("org.manatki", _.derevo, "derevo")("core", "circe", "tschema")

  lazy val chimney = lib("io.scalaland", _.chimney)("chimney")

  def lib(org: String, ver: version.type => String, prefix: String*)(names: String*): Seq[ModuleID] =
    names.map(name => org %% (prefix :+ name).mkString("-") % ver(version))

  def jlib(org: String, ver: version.type => String, prefix: String*)(names: String*): Seq[ModuleID] =
    names.map(name => org % (prefix :+ name).mkString("-") % ver(version))
}
